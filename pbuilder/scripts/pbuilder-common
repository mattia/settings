#!/bin/bash
# Remeber: there is also ~/.pbuilderrc with the config

set -e

debians=(squeezy wheezy jessie stretch buster testing sid unstable experimental)
ubuntus=(lucid maverick natty oneiric precise quantal raring saucy trusty utopic
         vivid wily devel)

DIST=$(basename "$0" | cut -f2 -d '-')
ARCH=$(basename "$0" | cut -f3,4 -d '-')
OPERATION=$1
COMMAND=($(echo "$@"|sed s/"$OPERATION"//))
PACKAGE=$(echo "${COMMAND[*]}" |awk -F/ '{print $NF}'|awk '{print $1}'|sed 's/\.dsc$//')
BASE_DIR="$HOME/pbuilder"
OTHER=()
BUILDER="pbuilder"
LIBEATMYDATA="eatmydata"

kind() {
    contains() {
        local n=$#
        local value=${!n}
        for ((i=1; i < $#; i++)) {
            if [ "${!i}" == "${value}" ]; then
                return 0
            fi
        }
        return 1
    }
    if (contains "${debians[@]}" "$DIST") ; then
        TYPE="debian"
    elif (contains "${ubuntus[@]}" "$DIST") ; then
        TYPE="ubuntu"
    else
        echo "Release do not know."
        exit 1
    fi
}

install_packages() {
    declare -a required_packages=("${1}")
    if [ ! -z "$LIBEATMYDATA" ] ; then
        required_packages+=("libc6:$ARCH" "libeatmydata1:$ARCH" "libselinux1:$ARCH")
    fi
    if [ "$ARCH" != "amd64" ] && [ "$ARCH" != "i386" ] ; then
        required_packages+=("qemu-user-static")
    fi
    if ! (dpkg --print-foreign-architectures | grep -q "$ARCH"); then
        echo "Please enable the $ARCH foreign architeture in dpkg."
        echo "e.g. \`sudo dpkg --add-architecture $ARCH\`"
        exit 1
    fi
    for package in "${required_packages[@]}" ; do
        dpkg-query -W -f='${Status}\n' "$package" 2>&1 | grep -q "install ok installed" \
            || echo "installing $package" && sudo apt-get -y install "$package" \
            || exit 1
    done
}

sett() {
    mkdir -vp "$BASE_DIR/logs/$DIST/$ARCH"
    mkdir -vp "$BASE_DIR/result/$DIST/$ARCH"
    OTHERMIRROR="deb [trusted=yes] file:///home/mattia/pbuilder/repository ./|deb [trusted=yes] http://ftp.mapreri.org/repo $DIST main"
    case $TYPE in
        debian)
            MIRROR="http://ftp.de.debian.org/debian"
            COMPONENTS=(main)
            KEYRING="/usr/share/keyrings/debian-archive-keyring.gpg"
            ;;
        ubuntu)
            MIRROR="http://archive.ubuntu.com/ubuntu"
            COMPONENTS=(main universe)
            KEYRING="/usr/share/keyrings/ubuntu-archive-keyring.gpg"
            ;;
    esac

    case $ARCH in
    amd64|i386)
        case "$TYPE" in
            ubuntu)
                ;;
            debian)
                case "$DIST" in
                    wheezy)
                        OTHERMIRROR="$OTHERMIRROR|deb [trusted=yes] http://ftp.mapreri.org/repo wheezy-backports main|deb http://security.debian.org/ wheezy/updates main|deb http://http.debian.net/debian wheezy-updates main|deb http://http.debian.net/debian wheezy-backports main"
                        ;;
                    jessie)
                        OTHERMIRROR="$OTHERMIRROR|deb [trusted=yes] http://ftp.mapreri.org/repo jessie-backports main|deb http://security.debian.org/ jessie/updates main|deb http://http.debian.net/debian jessie-updates main|deb http://http.debian.net/debian jessie-backports main"
                        ;;
                    *)
                        ;;
                    esac
                ;;
            esac
        BASE=(--basetgz $BASE_DIR/$DIST-$ARCH-base.tgz)
        ;;
    armhf|armel)
        arm_required_packages=("qemu-system-arm")
        install_packages "${arm_required_packages[@]}"
        if [ "$TYPE" = "ubuntu" ] ; then
            MIRROR="http://ports.ubuntu.com"
        fi
        OTHER+=(--debootstrap qemu-debootstrap)
        BASE=(--basetgz $BASE_DIR/$DIST-$ARCH-base.tgz)
        ;;
    arm64)
        arm64_required_packages=("qemu-system-arm")
        install_packages "${arm64_required_packages[@]}"
        case "$TYPE" in
            ubuntu)
                MIRROR="http://ports.ubuntu.com"
                OTHERMIRROR="deb [trusted=yes] http://ftp.mapreri.org/repo $DIST main universe"
                ;;
        esac
        OTHER+=(--debootstrap qemu-debootstrap)
        BASE=(--basetgz $BASE_DIR/$DIST-$ARCH-base.tgz)
	    ;;
    s390x)
        s390x_required_packages=("qemu-user" "qemu-system-misc")
        install_packages "${s390x_required_packages[@]}"
        OTHER+=(--debootstrap qemu-debootstrap)
        BASE=(--basetgz $BASE_DIR/$DIST-$ARCH-base.tgz)
        ;;
    esac
    if [ ! -d "$BASE_DIR/result/$DIST/$ARCH" ] ; then
        mkdir -p "$BASE_DIR/result/$DIST/$ARCH"
    fi
}

logcheck() {
    if [ -f "$LOGFILE" ] ; then
        echo
        blhc --all --color  "$LOGFILE" && echo Good News Everyone! Your build is perfect! :D || echo "Please fix!"
        echo
    fi
}

build() {
    if [ "$BUILDER" == "qemubuilder" ] ; then
        sudo modprobe loop
    fi
    set -x
    sudo -E DEB_BUILD_OPTIONS="${DEB_BUILD_OPTIONS:+"$DEB_BUILD_OPTIONS "}parallel=4" \
        $LIBEATMYDATA \
        "$BUILDER" --"$OPERATION" \
        --override-config \
        --architecture "$ARCH" \
        --distribution "$DIST" \
        --components "${COMPONENTS[*]}" \
        --http-proxy "http://localhost:3128" \
        --buildresult "$BASE_DIR/result/$DIST/$ARCH"/ \
        --debootstrapopts --keyring="$KEYRING" \
        --othermirror "$OTHERMIRROR" \
        --mirror "$MIRROR" \
        "${LOG[@]}" \
        "${BASE[@]}" \
        "${OTHER[@]}" \
        "${COMMAND[@]}"

    set +x
}

case $OPERATION in
    create)
        kind
        sett
        LOGFILE="$BASE_DIR/logs/$DIST/$ARCH/create-$(date +%Y%m%d-%H%M%S-%Z)"
        LOG=(--logfile $LOGFILE)
        build
        ;;
    update|up)
        kind
        sett
        LOGFILE="$BASE_DIR/logs/$DIST/$ARCH/update-$(date +%Y%m%d-%H%M%S-%Z)"
        LOG=(--logfile $LOGFILE)
        OPERATION="update"
        build
        ;;
    build)
        kind
        sett
        LOGFILE="$BASE_DIR/logs/$DIST/$ARCH/${PACKAGE}_$(date +%Y%m%d-%H%M%S-%Z)"
        LOG=(--logfile $LOGFILE)
        build
        logcheck
        notify-send "The ${PACKAGE} build has just finished!"
        ;;
    login|execute)
        kind
        sett
        build
        ;;
    *)
        echo "Invalid command."
        echo "Valid commands are:"
        echo "   create"
        echo "   update"
        echo "   build"
        echo "   login"
        echo "   execute"
        exit 1
        ;;
esac

